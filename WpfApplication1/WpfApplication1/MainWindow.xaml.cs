﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        List<UserCheckbox> lista = new List<UserCheckbox>();

        UserTextbox userTextbox;

        public MainWindow()
        {
            InitializeComponent();
            InitializeComobox();
            
        }

        private void InitializeComobox()
        {
            addUserControl();
            addCheckBox("pon");
            addCheckBox("wt");
        }

        void addUserControl()
        {
            userTextbox = new UserTextbox();
            userTextbox.button.Click += UserTextoxButton_Click;
            comboBox.Items.Add(userTextbox);
            
        }

        private void UserTextoxButton_Click(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(userTextbox.textBox.Text))
            {
                addCheckBox(userTextbox.textBox.Text);
            }
            comboBox.IsDropDownOpen = true;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("s");
        }

        void addCheckBox(string s)
        {

            var checkBox = new UserCheckbox();
            checkBox.checkBox.Content = s;
            lista.Add(checkBox);
            checkBox.buttonClicked += CheckBox_buttonClicked;
            comboBox.Items.Add(checkBox);
            userTextbox.textBox.Clear();
        }

        private void CheckBox_buttonClicked(object sender, RoutedEventArgs e)
        {
            UserTextbox ueserTextBox = sender as UserTextbox;
            if (userTextbox != null)
            {

            }
        }
    }
}
