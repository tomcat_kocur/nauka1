﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApplication1
{
    public partial class UserCheckbox : UserControl
    {
        public delegate void buttonClickHander(object sender, RoutedEventArgs e);
        public event buttonClickHander buttonClicked;

        public UserCheckbox()
        {
            InitializeComponent();
            button.Click += Button_Click;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            buttonClicked(sender, e);
        }
    }
}
